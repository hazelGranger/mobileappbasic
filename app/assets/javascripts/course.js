$(document).ready(function(){

	var courselist = new Vue({
		el:'#course-list',
		data: {
			list: [{
				name: '张悬教你弹吉他',
				ages: '5-7',
				time: '20 课时',
				instr: '吉他',
				price: '1200 元起'
			},{
				name: '张悬教你弹吉他',
				ages: '5-7',
				time: '25 课时',
				instr: '吉他',
				price: '1300 元起'
			},{
				name: '张悬教你弹吉他',
				ages: '5-7',
				time: '20 课时',
				instr: '吉他',
				price: '1200 元起'
			}]
		}
	})

	var coursedetail = new Vue({
		el: '#coursedetail',
		data: {
			course: {
				name: '张悬教你弹吉他',
				ages: '5-7',
				time: '20 课时',
				instr: '吉他',
				price: '1200 元',
				starttime: '2016-09-01',
				routine: '每周日 15:00 —— 16:00'
			}
		}
	})
});