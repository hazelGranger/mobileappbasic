//Vue.component('',{})

$(document).ready(function(){

	var steps = new Vue({
		el: '#sign',
		data: {
			curStep: 0,
			ages: "-1",
			instr: [],
			loc: "-1",
			tel: "",
			idcode: ""
		},
		methods: {
			next: function(){
				var that = this;
				that.curStep += 1;
			},
			prev: function(){
				var that = this;
				that.curStep -= 1;
			},
			sign: function(){
				var that = this;
				console.log(that.ages,that.instr,that.loc,that.tel,that.idcode);
				window.location.href = "/choose";
			}
		}
	})

});
