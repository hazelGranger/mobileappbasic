$(document).ready(function(){

	$('.tab .tab-item').click(function(){
	
		if (!$(this).hasClass('date')) {
			$('.tab .tab-item').removeClass("active");
			$(this).addClass('active');
			$('.timelist').hide();
			if ($(this).hasClass('am')) {
				$(this).closest('.day').find('.timelist.am').show();
			};
			if ($(this).hasClass('pm')) {
				$(this).closest('.day').find('.timelist.pm').show();
			};
			if ($(this).hasClass('night')) {
				$(this).closest('.day').find('.timelist.night').show();
			};
		};
		
	});
});