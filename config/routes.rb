Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  root 'index#welcome'

  get 'login' => 'login#index'

  get 'sign' => 'sign#index'


  get 'sign/step1' => 'sign#step1'
  get 'sign/step2' => 'sign#step2'
  get 'sign/step3' => 'sign#step3'

  get 'sign/send' => 'sign#send'

  get 'course' => 'course#index'

  get 'choose' => 'course#choose'

  get 'timefix' => 'course#timefix'

  get 'cousedetail' => 'course#cousedetail'

  get 'crcl' => 'curriculum#index'


  get 'willpay' => 'order#willpay'

  get 'willpaydialog' => 'order#willpaydialog'

  get 'orderpaid' => 'order#orderpaid'

  get 'paymethod' => 'order#paymethod'

  get 'payresult' => 'order#payresult'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
